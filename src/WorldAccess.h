/**
 * @file    src\WorldAccess.h
 *
 * @brief   Declares the world access class.
 */

#pragma once

#include "environment.h"
#include "Agent.h"

/**
 * @class   Agent
 *
 * @brief   Forward declaration for cross reference.
 */

class Agent;

/**
 * @class   WorldAccess
 *
 * @brief   An object used by Agent to interact with its environment
 */

class WorldAccess {
public:

    /**
     * @fn  WorldAccess::WorldAccess (Agent * agent, Environment * map, const std::pair<int, int> & pos);
     *
     * @brief   Constructor.
     *
     * @param [in,out]  agent   If non-null, the agent.
     * @param [in,out]  map     If non-null, the map.
     * @param   pos             The start position of the agent
     */

	WorldAccess (Agent * agent, Environment * map, const std::pair<int, int> & pos);

    /**
     * @fn  bool WorldAccess::move (int xDelta, int yDelta);
     *
     * @brief   Moves the agent in the given direction. Maximum of 1 tile at a time
     *
     * @param   xDelta  The x movement.
     * @param   yDelta  The y movement.
     *
     * @return  true if it succeeds, false if it fails.
     */

    bool move (int xDelta, int yDelta);

    /**
     * @fn  bool WorldAccess::move (const std::pair<int, int> & direction);
     *
     * @brief   Moves the agent in the given direction. Maximum of 1 tile at a time
     *
     * @param   direction   The direction to move in.
     *
     * @return  true if it succeeds, false if it fails.
     */

	bool move (const std::pair<int, int> & direction);

    /**
     * @fn  const std::pair<int, int> & WorldAccess::getPosition ();
     *
     * @brief   Gets the agent's current position.
     *
     * @return  The agent's current position.
     */

	const std::pair<int, int> & getPosition ();

    /**
     * @fn  const std::pair<int, int> & WorldAccess::getStartPosition ();
     *
     * @brief   Gets the agent's start position.
     *
     * @return  The agent's start position.
     */

	const std::pair<int, int> & getStartPosition ();

    /**
     * @fn  bool WorldAccess::getClean ();
     *
     * @brief   Gets the clean value from the tile the agent is on.
     *
     * @return  true if it is clean, false otherwise.
     */

	bool getClean ();

    /**
     * @fn  void WorldAccess::cleanTile ();
     *
     * @brief   Cleans the tile the agent is on.
     */

	void cleanTile ();
	
protected:

    /**
     * @fn  bool WorldAccess::getClean (int x, int y);
     *
     * @brief   Gets the clean value from the specified tile.
     *
     * @param   x   The x coordinate.
     * @param   y   The y coordinate.
     *
     * @return  true if it is clean, false otherwise.
     */

	bool getClean (int x, int y);

    /**
     * @fn  bool WorldAccess::getClean (const std::pair <int, int> & pos);
     *
     * @brief   Gets the clean value from the specified tile.
     *
     * @param   pos The position.
     *
     * @return  true if it is clean, false otherwise.
     */

	bool getClean (const std::pair <int, int> & pos);

    /**
     * @fn  void WorldAccess::cleanTile (int x, int y);
     *
     * @brief   Cleans the specified tile.
     *
     * @param   x   The x coordinate.
     * @param   y   The y coordinate.
     */

	void cleanTile (int x, int y);

    /**
     * @fn  void WorldAccess::cleanTile (const std::pair <int, int> & pos);
     *
     * @brief   Cleans the specified tile.
     *
     * @param   pos The x coordinate.
     */

    void cleanTile (const std::pair <int, int> & pos);

	std::pair<int, int> agentPos;
	std::pair<int, int> startPos;

	Agent * agent;
	Environment * map;

};