/**
 * @file    src\simple_reflex_agent.cpp
 *
 * @brief   Implements the simple reflex agent class.
 */

#include "simple_reflex_agent.h"
#include <iostream>

/**
 * @fn  SimpleReflexAgent::SimpleReflexAgent ()
 *
 * @brief   Default constructor.
 */

SimpleReflexAgent::SimpleReflexAgent () {
    directionX = 1;
    directionY = 1;
}

/**
 * @fn  void SimpleReflexAgent::nextStep ()
 *
 * @brief   Execute next AI logic step.
 */

void SimpleReflexAgent::nextStep () {
    if (!worldAccess->getClean()) { //if dirty, clean tile
        worldAccess->cleanTile();
    }
    else if (worldAccess->move(0, directionY)); // move in Y if possible
    else if (worldAccess->move(directionX, 0)); // move in X if possible
    else if (worldAccess->move(-directionX, 0)) {
        directionX *= -1; // flip X
        if (rand()%2 == 0) directionY *= -1; // 50% chance to flip Y
    } else if (worldAccess->move(0, -directionY)) {
        directionY *= -1; // flip Y
        if (rand()%2 == 0) directionX *= -1; // 50% chance to flip X
    }
    else noOp();
}

/**
 * @fn  void SimpleReflexAgent::noOp ()
 *
 * @brief   No operation.
 */

void SimpleReflexAgent::noOp () {
    std::cout << "Help! A stuck robot is an unhappy robot." << std::endl;
}