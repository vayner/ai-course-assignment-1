/**
 * @file    src\TwoTile.cpp
 *
 * @brief   Implements the two tile class.
 */

#include "TwoTile.h"

/**
 * @fn  TwoTile::TwoTile ()
 *
 * @brief   Default constructor. Generates the 2 tile based area
 */

TwoTile::TwoTile () {
    EnvironmentNode * left = new EnvironmentNode();
    EnvironmentNode * right = new EnvironmentNode();

    left->flat = true;
    right->flat = true;

    left->clean = (rand () % 2 == 0);
    right->clean = (rand () % 2 == 0);
    
    this->area->addNode (std::pair<int, int> (0, 0), left);
    this->area->addNode (std::pair<int, int> (0, 1), right);
}