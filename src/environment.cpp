/**
 * @file    src\environment.cpp
 *
 * @brief   Implements the environment class.
 */

#include "environment.h"
#include "SDL.h"
#include "Global.h"
#include "Utility.h"

/**
 * @fn  Environment::Environment ()
 *
 * @brief   Default constructor.
 */

Environment::Environment () {
	this->tileTexture = display->loadTexture ("textures/tile.png");
	this->dirtTexture = display->loadTexture ("textures/dirty.png");
	this->wallTexture = display->loadTexture ("textures/pit.png");

	area = new NodeGrid<EnvironmentNode>();
}

/**
 * @fn  Environment::~Environment ()
 *
 * @brief   Destructor.
 */

Environment::~Environment () {
    delete area;
}

/**
 * @fn  bool Environment::exists (const std::pair<int, int> & pos)
 *
 * @brief   Check if specified node exists at the given position.
 *
 * @param   pos The position.
 *
 * @return  true if it exists, false otherwise.
 */

bool Environment::exists (const std::pair<int, int> & pos) {
	return this->area->nodeExists (pos);
}

/**
 * @fn  bool Environment::getClean (const std::pair<int, int> & pos)
 *
 * @brief   Gets the clean value for the specified position.
 *
 * @param   pos The position.
 *
 * @return  true if it is clean, false otherwise.
 */

bool Environment::getClean (const std::pair<int, int> & pos) {
	return this->area->getData (pos)->clean;
}

/**
 * @fn  bool Environment::setClean (const std::pair<int, int> & pos, bool clean )
 *
 * @brief   Sets the clean value for specified node. Will return false if none exists at that
 *          position.
 *
 * @param   pos     The position to set value for.
 * @param   clean   Value to set clean to. Default true.
 *
 * @return  true if it succeeds, false if it fails.
 */

bool Environment::setClean (const std::pair<int, int> & pos, bool clean /* = true*/) {
	EnvironmentNode * ref = this->area->getData (pos);

	if (ref == NULL || ref->flat == false) {
		return false;
	} else {
        if (ref->clean != clean) {
            this->dirty += (clean == true) ? -1 :  1;
            this->clean += (clean == true) ?  1 : -1;
        }
		ref->clean = clean;
	}

	return true;
}

/**
 * @fn  bool Environment::getFlat (const std::pair<int, int> & pos)
 *
 * @brief   Gets the flat value for the specified position.
 *
 * @param   pos The position.
 *
 * @return  true if it is flat, false otherwise.
 */

bool Environment::getFlat (const std::pair<int, int> & pos) {
	return (this->area->nodeExists(pos))? this->area->getData (pos)->flat : false;
}

/**
 * @fn  bool Environment::setFlat (const std::pair<int, int> & pos, bool flat)
 *
 * @brief   Sets the flat value for specified node. Will return false if none exists at that
 *          position.
 *
 * @param   pos     The position to set value for.
 * @param   flat    Value to set flat to.
 *
 * @return  true if it succeeds, false if it fails.
 */

bool Environment::setFlat (const std::pair<int, int> & pos, bool flat) {
	EnvironmentNode * ref = this->area->getData (pos);

	if (ref == NULL) {
		return false;
	} else {
        if (ref->flat != flat) {
            this->blocks += (flat == true)? -1 :  1;
            this->clean  += (flat == true)?  1 : -1;
        }
        ref->flat = flat;
	}

	return true;
}

/**
 * @fn  void Environment::draw (const std::pair<int, int> &offset)
 *
 * @brief   Draws the area.
 *
 * @param   offset  The offset.
 */

void Environment::draw (const std::pair<int, int> &offset) {
	
	for (auto iterator = area->cbegin(); iterator != area->cend(); iterator++) {
		EnvironmentNode  * data = iterator->second;
		int id = (data->clean) ? tileTexture : dirtTexture;
		id = (data->flat) ? id : wallTexture;
        
		display->drawTileTexture (iterator->first + offset, id);
	}
}

/**
 * @fn  void Environment::count ()
 *
 * @brief   Counts the number of dirt, clean and blocked tiles.
 */

void Environment::count () {
    dirty = 0;
    clean = 0;
    blocks = 0;

    for (auto &EN : *area) {
        if (EN.second->flat == false) {
            ++blocks;
        } else if (EN.second->clean == false) {
            ++dirty;
        } else {
            ++clean;
        }
    }
}

/**
 * @fn  int Environment::getDirtyNumber ()
 *
 * @brief   Gets dirty number.
 *
 * @return  The dirty number.
 */

int Environment::getDirtyNumber () {
    return this->dirty;
}

/**
 * @fn  int Environment::getCleanNumber ()
 *
 * @brief   Gets clean number.
 *
 * @return  The clean number.
 */

int Environment::getCleanNumber () {
    return this->clean;
}

/**
 * @fn  int Environment::getBlocksNumber ()
 *
 * @brief   Gets blocks number.
 *
 * @return  The blocks number.
 */

int Environment::getBlocksNumber () {
    return this->blocks;
}