/**
 * @file    src\display.cpp
 *
 * @brief   Implements the display class.
 */

#include "display.h"
#include "Utility.h"

/**
 * @fn  Display::Display(const std::string & title, int width, int height)
 *
 * @brief   Constructor.
 *
 * @param   title   Window title.
 * @param   width   Window with.
 * @param   height  Window height.
 */

Display::Display(const std::string & title, int width, int height) {
    if (SDL_Init(SDL_INIT_EVERYTHING) != 0){
		utility::logSDLError(std::cout, "SDL_Init");
	}

	this->window = SDL_CreateWindow (title.c_str(), SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, width, height, SDL_WINDOW_SHOWN);

	if (this->window == NULL) {
		utility::logSDLError(std::cout, "CreateWindow");
	}

	this->renderer = SDL_CreateRenderer (this->window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

	if (this->renderer == NULL) {
		utility::logSDLError(std::cout, "CreateRenderer");
	}
}

/**
 * @fn  Display::~Display()
 *
 * @brief   Destructor.
 */

Display::~Display() {

    //cleanup
	SDL_DestroyRenderer(this->renderer);
	SDL_DestroyWindow(this->window);

	IMG_Quit();
	SDL_Quit();
}

/**
 * @fn  void Display::drawTexture (int x, int y, int textureID)
 *
 * @brief   Draw texture.
 *
 * @param   x           The x coordinate.
 * @param   y           The y coordinate.
 * @param   textureID   Identifier for the texture.
 */

void Display::drawTexture (int x, int y, int textureID) {
	utility::renderTexture (this->textures.at (textureID), this->renderer, x, y);
}

/**
 * @fn  void Display::drawTileTexture (int x, int y, int textureID, SDL_Color tint)
 *
 * @brief   Draw texture with grid coordinates. Grid size is the texture's size.
 *
 * @param   x           The x coordinate.
 * @param   y           The y coordinate.
 * @param   textureID   Identifier for the texture.
 * @param   tint        The tint to draw the texture with.
 */

void Display::drawTileTexture (int x, int y, int textureID, SDL_Color tint) {
	int width, height;
	SDL_QueryTexture (this->textures.at (textureID), NULL, NULL, &width, &height);
    SDL_SetTextureColorMod (this->textures.at (textureID), tint.r, tint.g, tint.b);
	utility::renderTexture (this->textures.at (textureID), this->renderer, x * width, y * height);
}

/**
 * @fn  void Display::drawTileTexture (const std::pair<int, int> &pos, int textureID, SDL_Color tint)
 *
 * @brief   Draw texture with grid coordinates. Grid size is the texture's size.
 *
 * @param   pos         The position.
 * @param   textureID   Identifier for the texture.
 * @param   tint        The tint to draw the texture with.
 */

void Display::drawTileTexture (const std::pair<int, int> &pos, int textureID, SDL_Color tint) {
    this->drawTileTexture (pos.first, pos.second, textureID, tint);
}

/**
 * @fn  void Display::update ()
 *
 * @brief   Updates the windows visual content.
 */

void Display::update () {
	SDL_RenderPresent (renderer);
    SDL_RenderClear (renderer);
}

/**
 * @fn  int Display::loadTexture (const std::string & path)
 *
 * @brief   Loads a texture.
 *
 * @param   path    Full pathname of the texture file.
 *
 * @return  The texture Identifier.
 */

int Display::loadTexture (const std::string & path) {
	if (this->texturePaths.find (path) != this->texturePaths.end ()) {
		return this->texturePaths[path];
	}

	this->textures.push_back (utility::loadTexture(path, this->renderer));

	return this->textures.size () - 1;
}
