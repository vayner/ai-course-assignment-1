/**
 * @file    src\ThinkAgent.cpp
 *
 * @brief   Implements the think agent class.
 */

#include "ThinkAgent.h"

#include "Utility.h"
#include "Global.h"

const int MOVECOST = 15;

/**
 * @fn  int getDistance (const std::pair<int, int> & a, const std::pair<int, int> & b)
 *
 * @brief   return the Manhattan distance between a and b positions.
 *
 * @param   a   The point A
 * @param   b   The point B
 *
 * @return  The distance.
 */

int getDistance (const std::pair<int, int> & a, const std::pair<int, int> & b) {
    return abs(a.first - b.first) + abs(a.second - b.second);
}

/**
 * @fn  ThinkAgent::ThinkAgent ()
 *
 * @brief   Default constructor.
 */

ThinkAgent::ThinkAgent () : position(0,0) {
    this->tileTexture = display->loadTexture ("textures/tile.png");
    this->wallTexture = display->loadTexture ("textures/pit.png");

    map.addNode (this->position, new NodeData ());
    unexplored.insert (this->position);

    for (auto &dir : DIRECTIONS) {
        unexplored.insert (this->position + dir);
    }
}

/**
 * @fn  ThinkAgent::~ThinkAgent ()
 *
 * @brief   Destructor.
 */

ThinkAgent::~ThinkAgent () {
}

/**
 * @fn  void ThinkAgent::nextStep ()
 *
 * @brief   Execute next AI logic step.
 */

void ThinkAgent::nextStep () {
    ++step;

    if (!worldAccess->getClean ()) {
        worldAccess->cleanTile ();
        return;
    }
    
    // generate orders if none exist
    if (orders.size () == 0) {
        this->pathfind( 
            (this->exploring)
            ? this->findUnexplored (this->position) // true
            : this->findOldest(this->position)      // false 
            );
    }

    auto order = orders.top ();
    orders.pop ();

    // execute order and check if it was possible, if not, generate new orders
    if (moveToNode (order) == false && orders.size () > 0) {
        while (!orders.empty ()) {
            orders.pop ();
        }
    }
}

/**
 * @fn  std::pair<int, int> ThinkAgent::findUnexplored (const std::pair<int, int> & position);
 *
 * @brief   Searches for the first unexplored relative to position
 *
 * @param   position    The position.
 *
 * @return  The selected unexplored position.
 */

std::pair<int, int> ThinkAgent::findUnexplored (const std::pair<int, int> & position) {
    std::pair<int, int> targetPos;
    int currentMin = INT_MAX;

    for (const std::pair<int, int> &p : unexplored) {
        int d = getDistance (p, position);
        

        if (d < currentMin) {
            currentMin = d;
            targetPos = p;
        }
    }

    return targetPos;
}

/**
 * @fn  std::pair<int, int> ThinkAgent::findOldest (const std::pair<int, int> & position);
 *
 * @brief   Searches for the oldest relative to position
 *
 * @param   position    The position.
 *
 * @return  The selected position.
 */

std::pair<int, int> ThinkAgent::findOldest (const std::pair<int, int> & position) {
    NodeData * target = nullptr;
    int currentMin = INT_MAX;
    oldVisit = INT_MAX;

    for (auto &p : map) {
        if (p.second->blocked == true) {
            continue;
        }

        int cost = p.second->lastVisit - step + exp2(getDistance (p.first, position)) * 0.01;
        if (cost < currentMin && p.second->blocked == false) {
            currentMin = cost;
            target = p.second;
        }

        oldVisit = (p.second->lastVisit < oldVisit) ? p.second->lastVisit : oldVisit;
    }

    return target->position;
}

/**
 * @fn  NodeData* lowestCost (const std::set<NodeData*> & list)
 *
 * @brief   Return the node with the lowest cost from the list.
 *
 * @param   list    The list.
 *
 * @return  null if it fails, else a NodeData*.
 */

NodeData* lowestCost (const std::set<NodeData*> & list) {
    NodeData* var = nullptr;
    int min = INT_MAX;

    for (auto &n : list) {
        if (n->startCost + n->goalCost < min) {
            min = n->startCost + n->goalCost;
            var = n;
        }
    }

    return var;
}

/**
 * @fn  int ThinkAgent::getCost (const NodeData* node, const std::pair<int, int> &pos) const
 *
 * @brief   Gets the cost for that node in relation to the position. Only used by the pathfinder.
 *
 * @param   node    The node to calculate it for.
 * @param   pos     The start position.
 *
 * @return  The cost.
 */

int ThinkAgent::getCost (const NodeData* node, const std::pair<int, int> &pos) const {
    int cost = node->lastVisit - this->oldVisit;

    if (node->parent == nullptr || node->position == pos) {
        return getDistance (node->position, pos) * MOVECOST + cost;
    }

    cost += this->getCost (node->parent, pos);

    return cost;
}

/**
 * @fn  void ThinkAgent::pathfind (const std::pair<int, int> & destination)
 *
 * @brief   Pathfinds to the given destination.
 *
 * @param   destination Position to pathfind to.
 */

void ThinkAgent::pathfind (const std::pair<int, int> & destination) {
    
    // nodes to work with currently
    std::set<NodeData*> open;
    std::set<NodeData*> closed;

    NodeData * current = nullptr;
    bool run = true;

    // get, update and add the start node
    NodeData * start = map.getData (this->position);
    start->goalCost = getDistance (start->position, destination) * MOVECOST;
    start->startCost = getCost (start, this->position);
    open.insert (start);

    while (run) {
        current = lowestCost (open);
        open.erase (open.find (current));
        closed.insert (current);

        // no pathfinder required if we are at the goal
        if (current->position == destination) {
            orders.push (destination);
            break;
        }

        for (auto &dir : DIRECTIONS) {
            auto nextNode = current->position + dir;

            // goal found beside us, finishing pathfinder
            if (nextNode == destination) {
                orders.push (destination);
                run = false;
                break;

                // wont walk over unknown nodes
            } else if (!map.nodeExists (nextNode)) {
                continue;
            }

            NodeData * temp = map.getData (nextNode);

            if (temp->blocked == false && closed.find (temp) == closed.end ()) {

                if (open.find(temp) == open.end()) {

                    open.insert (temp);

                    temp->parent = current;
                    temp->goalCost = getDistance (temp->position, destination) * MOVECOST;
                    temp->startCost = getCost (temp, this->position);

                } else if (temp->startCost > current->startCost) {
                    temp->parent = current;
                    temp->startCost = getCost (temp, this->position);
                }
            }
        }
    }

    // generate orders
    while (current != nullptr && current->position != this->position) {
        orders.push (current->position);
        current = current->parent;
    }
}

/**
 * @fn  bool ThinkAgent::moveToNode (const std::pair<int, int> & destination)
 *
 * @brief   Moves AI to node if possible.
 *
 * @param   destination Position to move to.
 *
 * @return  true if it succeeds, false if blocked or no movement.
 */

bool ThinkAgent::moveToNode (const std::pair<int, int> & destination) {
    bool result = worldAccess->move (destination - this->position);
    NodeData * data = map.getData (destination);

    
    if (data == nullptr) {
        data = new NodeData ();
        data->blocked = (result == false && destination != this->position);
        data->position = destination;
        map.addNode (data->position, data);
    }

    if (exploring == true) {
        auto origin = (result == true) ? destination : this->position;
        for (auto &dir : DIRECTIONS) {
            auto newDestination = origin + dir;

            if (map.nodeExists (newDestination) == true ||
                unexplored.find (newDestination) != unexplored.end ()) {
                continue;
            }

            unexplored.insert (newDestination);
        }
    }

    if (exploring == true && unexplored.size () == 0) {
        exploring = false;
    }

    if (result == true) {
        this->position = destination;
        data->lastVisit = this->step;
    }

    data->blocked = (result == false && destination != this->position);

    if (unexplored.find (destination) != unexplored.end ()) {
        unexplored.erase (unexplored.find (destination));
    }

    return result;
}

/**
 * @fn  void ThinkAgent::draw (const std::pair<int, int> &offset)
 *
 * @brief   Draws this object and its related data.
 *
 * @param   offset  The offset.
 */

void ThinkAgent::draw (const std::pair<int, int> &offset) {
    auto pos = worldAccess->getStartPosition (); //offset value for agent map translation

    for (auto &data : map) {
        if (pos.first + data.first.first < 0 ||
            pos.first + data.first.first >= WORLD_WIDTH ||
            pos.second + data.first.second < 0 ||
            pos.second + data.first.second >= WORLD_HEIGHT) {
            continue;
        }

        int look = (data.second->blocked) ? wallTexture : tileTexture;

        SDL_Color tint;

        if (look == wallTexture || data.second->lastVisit == -1 || exploring == true) {
            tint.r = 255;
            tint.g = 255;
            tint.b = 255;
        } else {
            tint.r = 255;
            tint.g = 255 * ((float) (data.second->lastVisit - oldVisit) / (step - oldVisit));
            tint.b = 255 * ((float) (data.second->lastVisit - oldVisit) / (step - oldVisit));
        }

        display->drawTileTexture (offset + pos + data.first, look, tint);
    }

    for (auto &nodes : unexplored) {
        if (pos.first + nodes.first < 0 ||
            pos.first + nodes.first >= WORLD_WIDTH ||
            pos.second + nodes.second < 0 ||
            pos.second + nodes.second >= WORLD_HEIGHT) {
            continue;
        }

        SDL_Color tint;

        tint.r = 155;
        tint.g = 155;
        tint.b = 155;

        display->drawTileTexture (pos + nodes + offset, tileTexture, tint);
    }
}