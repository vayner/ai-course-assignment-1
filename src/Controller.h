/**
 * @file    src\Controller.h
 *
 * @brief   Declares the controller class.
 */

#pragma once

#include "Global.h"
#include "WorldAccess.h"

/**
 * @class   Controller
 *
 * @brief   Handles AI <-> World <-> Main loop interaction 
 *
 * @sa  WorldAccess
 */

class Controller : public WorldAccess {
public:

    /**
     * @fn  Controller::Controller (Agent * agent, Environment * map, const std::pair<int, int> & pos);
     *
     * @brief   Constructor. Also sets Agent's worldAccess to itself.
     *
     * @param [in,out]  agent   If non-null, the agent.
     * @param [in,out]  map     If non-null, the map.
     * @param   pos             The agent's start position.
     */

	Controller (Agent * agent, Environment * map, const std::pair<int, int> & pos);

    /**
     * @fn  Controller::~Controller ();
     *
     * @brief   Destructor.
     */

	~Controller ();

    /**
     * @fn  int Controller::nextStep ();
     *
     * @brief   Executes next logic step for the AI.
     *
     * @return  An int.
     */

	int nextStep ();

    /**
     * @fn  bool Controller::generateRandomDirt ();
     *
     * @brief   Generates 1 random dirt.
     *
     * @return  true if it succeeds, false if it fails.
     */

    bool generateRandomDirt ();

    /**
     * @fn  bool Controller::generateDirt (const std::pair<int, int> & pos);
     *
     * @brief   Generates dirt at specified position.
     *
     * @param   pos The position.
     *
     * @return  true if it succeeds, false if it fails.
     */

    bool generateDirt (const std::pair<int, int> & pos);

    /**
     * @fn  void Controller::draw (bool drawAgentSide);
     *
     * @brief   Calls agent and maps draw function and draws the agent.
     *
     * @param   drawAgentSide   true to draw agent side.
     */

    void draw (bool drawAgentSide);

    /**
     * @fn  const std::pair<int, int> & Controller::getOffset () const;
     *
     * @brief   Gets the offset.
     *
     * @return  The offset.
     */

    const std::pair<int, int> & getOffset () const;

    /**
     * @fn  void Controller::setOffset (const std::pair<int, int> & offset);
     *
     * @brief   Sets an offset.
     *
     * @param   offset  The offset.
     */

    void setOffset (const std::pair<int, int> & offset);

private:

	int currentStep = 1;
	int agentTexture;

    std::pair<int, int> offset;
    std::pair<int, int> displacement;
};