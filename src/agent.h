/**
 * @file    src\Agent.h
 *
 * @brief   Declares the agent class.
 */

#pragma once
#include "WorldAccess.h"

/**
 * @class   WorldAccess
 *
 * @brief   Forward declaration for cross reference.
 */

class WorldAccess;

/**
 * @class   Agent
 *
 * @brief   An abstract base agent. Used as a base for AI for easier management of multiple types
 *          of AI's.
 */

class Agent {
public:

    /**
     * @fn  Agent::Agent ();
     *
     * @brief   Default constructor.
     */

	Agent ();

    /**
     * @fn  virtual Agent::~Agent ();
     *
     * @brief   Destructor.
     */

    virtual ~Agent ();

    /**
     * @fn  void Agent::setAccess (WorldAccess * worldAccess);
     *
     * @brief   Sets the worldAccess reference
     *
     * @param [in,out]  worldAccess If non-null, the world access.
     */

	void setAccess (WorldAccess * worldAccess);

    /**
     * @fn  virtual void Agent::nextStep() =0;
     *
     * @brief   Execute next AI logic step.
     */

	virtual void nextStep() =0;

    /**
     * @fn  virtual void Agent::draw (const std::pair<int, int> &offset);
     *
     * @brief   Draws this object.
     *
     * @param   offset  The offset.
     */

	virtual void draw (const std::pair<int, int> &offset);

protected:

	WorldAccess * worldAccess;
};