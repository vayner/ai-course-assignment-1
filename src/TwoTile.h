/**
 * @file    src\TwoTile.h
 *
 * @brief   Declares the two tile class.
 */

#pragma once
#include "environment.h"

/**
 * @class   TwoTile
 *
 * @brief   A 2 tile area as specified in assignment
 *
 * @sa  Environment
 */

class TwoTile : public Environment {
public:

    /**
     * @fn  TwoTile::TwoTile ();
     *
     * @brief   Default constructor. Generates the 2 tile based area.
     */

    TwoTile ();
    
};