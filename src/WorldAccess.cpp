/**
 * @file    src\WorldAccess.cpp
 *
 * @brief   Implements the world access class.
 */

#include "WorldAccess.h"
#include <cstdlib>
#include "Utility.h"

/**
 * @fn  WorldAccess::WorldAccess (Agent * agent, Environment * map, const std::pair<int, int> & pos)
 *
 * @brief   Constructor.
 *
 * @param [in,out]  agent   If non-null, the agent.
 * @param [in,out]  map     If non-null, the map.
 * @param   pos             The start position of the agent.
 */

WorldAccess::WorldAccess (Agent * agent, Environment * map, const std::pair<int, int> & pos) : agent (agent), map (map), agentPos(pos) {
	this->agent->setAccess (this);
	this->startPos = this->agentPos;
}

/**
 * @fn  bool WorldAccess::move (int xDelta, int yDelta)
 *
 * @brief   Moves the agent in the given direction. Maximum of 1 tile at a time.
 *
 * @param   xDelta  The x movement.
 * @param   yDelta  The y movement.
 *
 * @return  true if it succeeds, false if it fails.
 */

bool WorldAccess::move (int xDelta, int yDelta) {
	return this->move (std::pair<int, int> (xDelta, yDelta));
}

/**
 * @fn  bool WorldAccess::move (const std::pair<int, int> & direction)
 *
 * @brief   Moves the agent in the given direction. Maximum of 1 tile at a time.
 *
 * @param   direction   The direction to move in.
 *
 * @return  true if it succeeds, false if it fails.
 */

bool WorldAccess::move (const std::pair<int, int> & direction) {

	// checks if the move 1 in difference and not the same tile
	if ((abs (direction.first) + abs (direction.second)) != 1) {
		return false;
	}

	auto newPos = this->agentPos + direction;

	if (this->map->getFlat (newPos)) {
		this->agentPos = newPos;
		return true;
	}

	return false;
}

/**
 * @fn  const std::pair<int, int> & WorldAccess::getPosition ()
 *
 * @brief   Gets the agent's current position.
 *
 * @return  The agent's current position.
 */

const std::pair<int, int> & WorldAccess::getPosition () {
	return this->agentPos;
}

/**
 * @fn  const std::pair<int, int> & WorldAccess::getStartPosition ()
 *
 * @brief   Gets the agent's start position.
 *
 * @return  The agent's start position.
 */

const std::pair<int, int> & WorldAccess::getStartPosition () {
	return this->startPos;
}

/**
 * @fn  bool WorldAccess::getClean ()
 *
 * @brief   Gets the clean value from the tile the agent is on.
 *
 * @return  true if it is clean, false otherwise.
 */

bool WorldAccess::getClean () {
	return this->getClean (this->agentPos);
}

/**
 * @fn  bool WorldAccess::getClean (int x, int y)
 *
 * @brief   Gets the clean value from the specified tile.
 *
 * @param   x   The x coordinate.
 * @param   y   The y coordinate.
 *
 * @return  true if it is clean, false otherwise.
 */

bool WorldAccess::getClean (int x, int y) {
	return this->getClean (std::pair<int, int> (x, y));
}

/**
 * @fn  bool WorldAccess::getClean (const std::pair<int, int> & pos)
 *
 * @brief   Gets the clean value from the specified tile.
 *
 * @param   pos The position.
 *
 * @return  true if it is clean, false otherwise.
 */

bool WorldAccess::getClean (const std::pair<int, int> & pos) {
	return this->map->getClean (pos);
}

/**
 * @fn  void WorldAccess::cleanTile ()
 *
 * @brief   Cleans the tile the agent is on.
 */

void WorldAccess::cleanTile () {
	return this->cleanTile (this->agentPos);
}

/**
 * @fn  void WorldAccess::cleanTile (int x, int y)
 *
 * @brief   Cleans the specified tile.
 *
 * @param   x   The x coordinate.
 * @param   y   The y coordinate.
 */

void WorldAccess::cleanTile (int x, int y) {
	this->cleanTile (std::pair<int, int> (x, y));
}

/**
 * @fn  void WorldAccess::cleanTile (const std::pair<int, int> & pos)
 *
 * @brief   Cleans the specified tile.
 *
 * @param   pos The position.
 */

void WorldAccess::cleanTile (const std::pair<int, int> & pos) {
	this->map->setClean (pos);
}
