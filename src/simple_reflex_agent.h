/**
 * @file    src\simple_reflex_agent.h
 *
 * @brief   Declares the simple reflex agent class.
 */

#pragma once

#include "Agent.h"
#include "WorldAccess.h"

/**
 * @class   SimpleReflexAgent
 *
 * @brief   A simple reflex agent.
 *
 * @sa  Agent
 */

class SimpleReflexAgent : public Agent {
public:

    /**
     * @fn  SimpleReflexAgent::SimpleReflexAgent ();
     *
     * @brief   Default constructor.
     */

	SimpleReflexAgent ();

    /**
     * @fn  void SimpleReflexAgent::nextStep ();
     *
     * @brief   Execute next AI logic step.
     */

	void nextStep ();

    /**
     * @fn  void SimpleReflexAgent::noOp ();
     *
     * @brief   No operation.
     */

    void noOp ();

private:
    int directionX;
    int directionY;


};