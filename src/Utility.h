/**
 * @file    src\Utility.h
 *
 * @brief   Declares the utility namespace functions.
 */

#pragma once
#include <iostream>
#include <string>

#include "SDL.h"
#include "SDL_image.h"

namespace utility {

    /**
     * @fn  void logSDLError(std::ostream &os, const std::string &msg);
     *
     * @brief   Logs sdl error.
     *
     * @param [in,out]  os  The output stream to write the message too.
     * @param   msg         The error message to write, format will be msg error: SDL_GetError()
     */

	void logSDLError(std::ostream &os, const std::string &msg);

    /**
     * @fn  void renderTexture(SDL_Texture *tex, SDL_Renderer *ren, int x, int y);
     *
     * @brief   Renders the texture.
     *
     * @param [in,out]  tex The source texture we want to draw.
     * @param [in,out]  ren The renderer we want to draw too.
     * @param   x           The x coordinate to draw too.
     * @param   y           The y coordinate to draw too.
     */

	void renderTexture(SDL_Texture *tex, SDL_Renderer *ren, int x, int y);

    /**
     * @fn  SDL_Texture* loadTexture(const std::string &file, SDL_Renderer *ren);
     *
     * @brief   Loads a texture.
     *
     * @param   file        The image file to load.
     * @param [in,out]  ren The renderer to load the texture onto.
     *
     * @return  the loaded texture, or nullptr if something went wrong.
     */

	SDL_Texture* loadTexture(const std::string &file, SDL_Renderer *ren);

    /**
     * @fn  int getNumber (const std::string &message, int min, int max, bool showLimit = true);
     *
     * @brief   Gets a number.
     *
     * @param   message     The message to display in the console for the request.
     * @param   min         The minimum limiter for the input.
     * @param   max         The maximum limiter for the input.
     * @param   showLimit   (Optional) Option to display the limits or not right after the message,
     *                      defaul true.
     *
     * @return  A number that is &gt;= min and &lt;= max.
     */

	int getNumber (const std::string &message, int min, int max, bool showLimit = true);

}

/**
 * @fn  std::pair<int, int> operator+ (const std::pair<int, int> & l, const std::pair<int, int> & r);
 *
 * @brief   Addition operator for std::pair&lt;int, int&gt;
 *
 * @param   l   The first value.
 * @param   r   The second value.
 *
 * @return  The result of the operation.
 */

std::pair<int, int> operator+ (const std::pair<int, int> & l, const std::pair<int, int> & r);

/**
 * @fn  std::pair<int, int> operator- (const std::pair<int, int> & l, const std::pair<int, int> & r);
 *
 * @brief   Subtraction operator for std::pair&lt;int, int&gt;
 *
 * @param   l   The first value.
 * @param   r   The second value.
 *
 * @return  The result of the operation.
 */

std::pair<int, int> operator- (const std::pair<int, int> & l, const std::pair<int, int> & r);