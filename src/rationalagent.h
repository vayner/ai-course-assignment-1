/**
 * @file    src\rationalagent.h
 *
 * @brief   Declares the rationalagent class.
 */

#pragma once
#include <vector>
#include <queue>
#include "Agent.h"
#include "NodeGrid.h"

const int DIRX[] = {1,0,0,-1};
const int DIRY[] = {0,1,-1,0};

struct MemoryNode {
    bool accessible = true;
    bool explored = false;
    int dir = -1;
    bool searched = false;

    int x = 0;
    int y = 0;
    int cost;
};

class RationalAgent : public Agent {
    public:
        /** Default constructor */
        RationalAgent();
        /** Default destructor */
        virtual ~RationalAgent();
        virtual void nextStep();
        virtual void draw (const std::pair<int, int> &offset);
        void pathFinding();
    protected:
        void visitNode();

        NodeGrid<MemoryNode> memory;
        int x = 0;
        int y = 0;
        bool hasExploredArea = false;
        bool hasPath = false;
        int goalX = 0;
        int goalY = 0;
        bool floorClean = false;
        int maxX = 0;
        int minX = 0;
        int maxY = 0;
        int minY = 0;


		int tileTexture;
		int wallTexture;


    private:

};
