/**
 * @file    src\Utility.cpp
 *
 * @brief   Implements the utility namespace functions.
 */

#include "Utility.h"

/**
 * @fn  void utility::logSDLError (std::ostream &os, const std::string &msg)
 *
 * @brief   Logs sdl error.
 *
 * @param [in,out]  os  The output stream to write the message too.
 * @param   msg         The error message to write, format will be msg error: SDL_GetError()
 */

void utility::logSDLError (std::ostream &os, const std::string &msg) {
	os << msg << " error: " << SDL_GetError() << std::endl;
}

/**
 * @fn  void utility::renderTexture (SDL_Texture *tex, SDL_Renderer *ren, int x, int y)
 *
 * @brief   Renders the texture.
 *
 * @param [in,out]  tex The source texture we want to draw.
 * @param [in,out]  ren The renderer we want to draw too.
 * @param   x           The x coordinate to draw too.
 * @param   y           The y coordinate to draw too.
 */

void utility::renderTexture (SDL_Texture *tex, SDL_Renderer *ren, int x, int y) {
	//Setup the destination rectangle to be at the position we want
	SDL_Rect dst;
	dst.x = x;
	dst.y = y;
	//Query the texture to get its width and height to use
	SDL_QueryTexture(tex, NULL, NULL, &dst.w, &dst.h);
	SDL_RenderCopy(ren, tex, NULL, &dst);
}

/**
 * @fn  SDL_Texture* utility::loadTexture (const std::string &file, SDL_Renderer *ren)
 *
 * @brief   Loads a texture.
 *
 * @param   file        The image file to load.
 * @param [in,out]  ren The renderer to load the texture onto.
 *
 * @return  the loaded texture, or nullptr if something went wrong.
 */

SDL_Texture* utility::loadTexture (const std::string &file, SDL_Renderer *ren) {
	SDL_Texture *texture = IMG_LoadTexture(ren, file.c_str());
	if (texture == NULL)
		logSDLError(std::cout, "LoadTexture");
	return texture;
}

/**
 * @fn  int utility::getNumber (const std::string &message, int min, int max, bool showLimit )
 *
 * @brief   Gets a number.
 *
 * @param   message     The message to display in the console for the request.
 * @param   min         The minimum limiter for the input.
 * @param   max         The maximum limiter for the input.
 * @param   showLimit   Option to display the limits or not right after the message, default true.
 *
 * @return  A number that is &gt;= min and &lt;= max.
 */

int utility::getNumber (const std::string &message, int min, int max, bool showLimit /*= true*/) {
	if (min > max) {
		std::cerr << "Ilegal operation, minimum value for getNumber cant be larger than maximum" << std::endl;
		return INT_MIN;
	}

	std::string limitMessage = " [ " + std::to_string (min) + " - " + std::to_string (max) + " ]: ";

	int number = INT_MIN;
	std::string input;
	bool valid;
	
	do {

		valid = false;
		std::cout << message << ((showLimit) ? (limitMessage) : "");
		std::getline (std::cin, input);
		
		try {
			number = std::stoi (input);
			valid = true;
		} catch (std::invalid_argument e) {
		} catch (std::out_of_range e) {
		}

	} while (!valid || number < min || number > max);

	return number;
}

/**
 * @fn  std::pair<int, int> operator+ (const std::pair<int, int> & l, const std::pair<int, int> & r)
 *
 * @brief   Addition operator for std::pair&lt;int, int&gt;
 *
 * @param   l   The first value.
 * @param   r   The second value.
 *
 * @return  The result of the operation.
 */

std::pair<int, int> operator+ (const std::pair<int, int> & l, const std::pair<int, int> & r) {
	return std::pair<int, int> (l.first + r.first, l.second + r.second);
}

/**
 * @fn  std::pair<int, int> operator- (const std::pair<int, int> & l, const std::pair<int, int> & r)
 *
 * @brief   Subtraction operator for std::pair&lt;int, int&gt;
 *
 * @param   l   The first value.
 * @param   r   The second value.
 *
 * @return  The result of the operation.
 */

std::pair<int, int> operator- (const std::pair<int, int> & l, const std::pair<int, int> & r) {
	return std::pair<int, int> (l.first - r.first, l.second - r.second);
}