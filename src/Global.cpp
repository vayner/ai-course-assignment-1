/**
 * @file    src\Global.cpp
 *
 * @brief   Implements some globally accessible values.
 */

#include "Global.h"

const std::pair<int, int> DIRECTIONS[4] {
    std::pair<int, int> (1, 0),
        std::pair<int, int> (-1, 0),
        std::pair<int, int> (0, 1),
        std::pair<int, int> (0, -1)
};