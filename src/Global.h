/**
 * @file    src\Global.h
 *
 * @brief   Declares the globally accessible values.
 */

#pragma once
#include "display.h"

/**
 * @brief   The main window.
 */

extern Display * display;

extern const int WORLD_WIDTH;
extern const int WORLD_HEIGHT;
extern const int SCREEN_WIDTH;
extern const int SCREEN_HEIGHT;

extern const std::pair<int, int> DIRECTIONS[4];