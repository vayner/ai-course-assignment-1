#pragma once
#include <vector>
#include <map>

template<typename T>
class NodeGrid {
public:

    /**
     * @fn  NodeGrid::NodeGrid ()
     *
     * @brief   Default constructor.
     */

	NodeGrid () {
	};

    /**
     * @fn  NodeGrid::~NodeGrid ()
     *
     * @brief   Destructor.
     */

	~NodeGrid () {
		for (auto &n : mapping) {
			delete n.second;
		}
	};

    /**
     * @fn  void NodeGrid::addNode (int x, int y, T * data)
     *
     * @brief   adds / updates a node with the given position and data.
     *
     * @param   x               The x coordinate.
     * @param   y               The y coordinate.
     * @param [in,out]  data    If non-null, the data.
     */

	void addNode (int x, int y, T * data) {
		this->addNode (std::pair<int, int> (x, y), data);
	}

    /**
     * @fn  void NodeGrid::addNode (const std::pair<int, int> & pos, T * data)
     *
     * @brief   adds / updates a node with the given position and data.
     *
     * @param   pos             The position.
     * @param [in,out]  data    If non-null, the data.
     */

	void addNode (const std::pair<int, int> & pos, T * data) {
		mapping[pos] = data;

	};

    /**
     * @fn  T* NodeGrid::getData (int x, int y)
     *
     * @brief   returns the node at the given position, if no node exists nullptr is returned instead.
     *
     * @param   x   The x coordinate.
     * @param   y   The y coordinate.
     *
     * @return  null if it fails, else the data.
     */

	T* getData (int x, int y) {
		return this->getData (std::pair<int, int> (x, y));
	}

    /**
     * @fn  T* NodeGrid::getData (const std::pair<int, int> & pos)
     *
     * @brief   returns the node at the given position, if no node exists nullptr is returned instead.
     *
     * @param   pos The position.
     *
     * @return  null if it fails, else the data.
     */

	T* getData (const std::pair<int, int> & pos) {
		return (this->nodeExists (pos)) ? mapping[pos] : nullptr;
	}

    /**
     * @fn  bool NodeGrid::nodeExists (int x, int y)
     *
     * @brief   checks if there exists a node at given position.
     *
     * @param   x   The x coordinate.
     * @param   y   The y coordinate.
     *
     * @return  true if it succeeds, false if it fails.
     */

	bool nodeExists (int x, int y) {
		return this->nodeExists (std::pair<int, int> (x, y));
	};

    /**
     * @fn  bool NodeGrid::nodeExists (const std::pair<int, int> & pos)
     *
     * @brief   checks if there exists a node at given position.
     *
     * @param   pos The position.
     *
     * @return  true if it succeeds, false if it fails.
     */

	bool nodeExists (const std::pair<int, int> & pos) {
		return !(mapping.find (pos) == mapping.end ());
	};

    /**
     * @fn  void NodeGrid::removeNode (const std::pair<int, int> & pos)
     *
     * @brief   Removes the node described by pos.
     *
     * @param   pos The position of the data to erase.
     */

    void eraseNode (const std::pair<int, int> & pos) {
        return mapping.erase (pos);
    }

	typename std::map<std::pair<int, int>, T*>::iterator begin () {
		return mapping.begin ();
	}

	typename std::map<std::pair<int, int>, T*>::iterator end () {
		return mapping.end ();
	}

    typename std::map<std::pair<int, int>, T*>::const_iterator begin () const {
        return mapping.begin ();
    }

    typename std::map<std::pair<int, int>, T*>::const_iterator end () const {
        return mapping.end ();
    }

	typename std::map<std::pair<int, int>, T*>::const_iterator cbegin () const {
		return mapping.cbegin ();
	}

	typename std::map<std::pair<int, int>, T*>::const_iterator cend () const {
		return mapping.cend ();
	}

private:

	std::map <std::pair<int, int>, T* > mapping;

};
