/**
 * @file    src\rationalagent.cpp
 *
 * @brief   Implements the rationalagent class.
 */

#include "rationalagent.h"

#include <iostream>
#include <algorithm>

#include "global.h"

bool nodeCompare (MemoryNode * nodeA, MemoryNode * nodeB) {
	return (nodeA->cost > nodeB->cost);
}


RationalAgent::RationalAgent(){

	this->tileTexture = display->loadTexture ("textures/tile.png");
	this->wallTexture = display->loadTexture ("textures/pit.png");

	MemoryNode * startNode = new MemoryNode ();

	startNode->x = 0;
	startNode->y = 0;

    memory.addNode(0,0, startNode );
    visitNode();
}

RationalAgent::~RationalAgent(){
}

void RationalAgent::draw (const std::pair<int, int> &offset) {
	auto pos = worldAccess->getStartPosition ();
	
	for (auto &data : memory) {
		if (pos.first + data.first.first < -1) {
			continue;
		}
		
		int look = (data.second->accessible) ? tileTexture : wallTexture;
		SDL_Color tint;



		tint.r = (data.second->explored || !data.second->accessible) ? 255 : 155;
		tint.g = (data.second->explored || !data.second->accessible) ? 255 : 155;
		tint.b = (data.second->searched) ? 150 : 255;

		display->drawTileTexture (pos.first + data.first.first + WORLD_WIDTH + 2, pos.second + data.first.second + 1, look, tint);
	}
}


void RationalAgent::visitNode() {
    MemoryNode * curNode = memory.getData(x,y);
    if ( !curNode->explored ) {
        curNode->explored = true;
        for ( int i = 0 ; i < 4 ; i++ ) {
            if( !memory.nodeExists(x + DIRX[i], y + DIRY[i])){
                MemoryNode * tmp = new MemoryNode();
                tmp->x = x+DIRX[i];
                tmp->y = y+DIRY[i];
                memory.addNode(x + DIRX[i], y + DIRY[i], tmp );
            }
        }
    }
}



void RationalAgent::pathFinding() {
    bool continueSearch = true;
    std::vector<MemoryNode*> openList;
    std::vector<MemoryNode*>::iterator it;
    MemoryNode * curNode = memory.getData(x,y);
    curNode->cost = 0;
    openList.push_back(curNode);
    while( continueSearch ) {
        std::sort (openList.begin(), openList.end(), nodeCompare);
        if( openList.empty() ) {
            floorClean = true;
            continueSearch = false;
        } else {
            curNode = openList.back();
            openList.pop_back();
            if ( curNode->explored == true ) {
                MemoryNode * tmpNode;
                for ( int i = 0 ; i < 4 ; i++ ) {
                    tmpNode = memory.getData( curNode->x+DIRX[i], curNode->y+DIRY[i] );

                    if ( tmpNode != nullptr && tmpNode->accessible ) {

                        int cost = curNode->cost + 1;
                        if ( tmpNode->cost > cost || !tmpNode->searched ) {

                            tmpNode->cost = curNode->cost + 1;
                            tmpNode->dir =3-i;  //points to the node before it
                            if( !tmpNode->searched ){
                                tmpNode->searched = true;
                                openList.push_back(tmpNode);
                            }
                        }
                    }
                }
            } else {
                continueSearch = false;         //Found an unexplored node
            }
        }

    }

    int dirA;
    int dirB;
    dirA = curNode->dir;
    curNode->dir = -1;              //signifies goal
    while ( curNode->x != x || curNode->y != y ) {
        curNode = memory.getData( curNode->x+DIRX[dirA], curNode->y+DIRY[dirA] );
        dirB = curNode->dir;
        curNode->dir = 3 - dirA;
        dirA = dirB;
    }

    for ( int i = -20 ; i < 20 ; i++ ) {        //TODO: de-uglyfy
        for ( int j = -20 ; j < 20 ; j++ ) {
            if( memory.nodeExists(i,j) ) {
                memory.getData(i,j)->searched = false;
            }
        }
    }


     //std::map<std::pair<int, int>, MemoryNode>::const_iterator a;
     //a = memory.cbegin;

}

void RationalAgent::nextStep() {
    if ( !floorClean ){                 //checks for mission complete
        if (!worldAccess->getClean()) {
            worldAccess->cleanTile();   //clean the tile
        } else {
            int dir;
            if ( !hasPath) {
                pathFinding();
                hasPath = true;
            }

            dir = memory.getData(x,y)->dir;
            if ( dir == -1 ) {
                hasPath = false;
            } else if( worldAccess->move( DIRX[dir], DIRY[dir] ) ) {
                x += DIRX[dir];
                y += DIRY[dir];
                visitNode();
            } else {
                memory.getData(x + DIRX[dir], y + DIRY[dir])->accessible = false;
                hasPath = false;
            }
        }
    }
}

