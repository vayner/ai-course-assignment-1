/**
 * @file    src\ThinkAgent.h
 *
 * @brief   Declares the think agent class.
 */

#pragma once

#include "agent.h"
#include "NodeGrid.h"

#include <vector>
#include <stack>
#include <set>

/**
 * @struct  NodeData
 *
 * @brief   A node data.
 */

struct NodeData {
    bool blocked = false;
    int lastVisit = -1;

    // used by pathfinder
    int goalCost = 0;
    int startCost = 0;
    std::pair<int, int> position;
    NodeData * parent = nullptr;
};

/**
 * @class   ThinkAgent
 *
 * @brief   A rational agent
 *
 * @sa  Agent
 */

class ThinkAgent : public Agent {
public:

    /**
     * @fn  ThinkAgent::ThinkAgent ();
     *
     * @brief   Default constructor.
     */

    ThinkAgent ();

    /**
     * @fn  virtual ThinkAgent::~ThinkAgent ();
     *
     * @brief   Destructor.
     */

    virtual ~ThinkAgent ();

    /**
     * @fn  void ThinkAgent::nextStep ()
     *
     * @brief   Execute next AI logic step.
     */

    virtual void nextStep ();

    /**
     * @fn  virtual void ThinkAgent::draw (const std::pair<int, int> &offset);
     *
     * @brief   Draws this object and its related data.
     *
     * @param   offset  The offset.
     */

    virtual void draw (const std::pair<int, int> &offset);

protected:

    /**
     * @fn  std::pair<int, int> ThinkAgent::findUnexplored (const std::pair<int, int> & position);
     *
     * @brief   Searches for the first unexplored relative to position
     *
     * @param   position    The position.
     *
     * @return  The selected unexplored position.
     */

    std::pair<int, int> findUnexplored (const std::pair<int, int> & position);

    /**
     * @fn  std::pair<int, int> ThinkAgent::findOldest (const std::pair<int, int> & position);
     *
     * @brief   Searches for the oldest relative to position
     *
     * @param   position    The position.
     *
     * @return  The selected position.
     */

    std::pair<int, int> findOldest (const std::pair<int, int> & position);

    /**
     * @fn  void ThinkAgent::pathfind (const std::pair<int, int> & destination);
     *
     * @brief   Pathfinds to the given destination.
     *
     * @param   destination Position to pathfind to
     */

    void pathfind (const std::pair<int, int> & destination);

    /**
     * @fn  bool ThinkAgent::moveToNode (const std::pair<int, int> & destination);
     *
     * @brief   Moves AI to node if possible.
     *
     * @param   destination Position to move to
     *
     * @return  true if it succeeds, false if blocked or no movement.
     */

    bool moveToNode (const std::pair<int, int> & destination);

    /**
     * @fn  int ThinkAgent::getCost (const NodeData* node, const std::pair<int, int> &pos) const;
     *
     * @brief   Gets the cost for that node in relation to the position. Only used by the pathfinder
     *
     * @param   node    The node to calculate it for
     * @param   pos     The start position.
     *
     * @return  The cost.
     */

    int getCost (const NodeData* node, const std::pair<int, int> &pos) const;

    int step;
    int oldVisit = 0;
    bool exploring = true;

    std::pair<int, int> position;

    NodeGrid<NodeData> map;
    std::stack<std::pair<int, int>> orders;
    std::set<std::pair<int, int>> unexplored;

    int tileTexture;
    int wallTexture;
};