/**
 * @file    src\main.cpp
 *
 * @brief   Implements the main function.
 */
#include <fstream>
#include <iostream>
#include <string>
#include <chrono>

#include "SDL.h"

#include "Global.h"
#include "display.h"
#include "Utility.h"

#include "Controller.h"
#include "SquareMap.h"

#include "simple_reflex_agent.h"
#include "rationalagent.h"
#include "ThinkAgent.h"

Display * display;

Controller * controller;
Agent * agent;
Environment * map;

// For the useless AI
Controller * derpController;
Agent * derpAgent;
Environment * derpMap;

const int TILE_SIZE = 32;
const int WORLD_WIDTH = 20;
const int WORLD_HEIGHT = 10;
const int SCREEN_WIDTH = 1280;//TILE_SIZE * WORLD_WIDTH;
const int SCREEN_HEIGHT = 720;//TILE_SIZE * WORLD_HEIGHT;

const int TICKS_PER_DIRT = 50;
bool randomDirt = false;
int delayTime = 30;
int cutOff = -1;

/**
 * @fn  void loop ()
 *
 * @brief   Main logic loop for this application.
 */

void loop () {

	bool running = true;
	SDL_Event sdlEvent;
    int currentStep = 0;
    int score = 0;

	// main loop
    while ( running ) {
		while (SDL_PollEvent (&sdlEvent)) {
			if (sdlEvent.type == SDL_QUIT) {
				running = false;
			}
		}

        if (randomDirt == true && currentStep % TICKS_PER_DIRT == 0) {
            controller->generateRandomDirt ();
            derpController->generateRandomDirt ();
        }

        ++currentStep;

        if (controller->nextStep () != derpController->nextStep ()) {
            running = false;
        }

        score += map->getCleanNumber ();
        std::cout << '\r' << currentStep << '\t' << score;

		// visual update
		controller->draw (true);
        derpController->draw (false);
		display->update ();


        if (currentStep >= cutOff && cutOff != -1) {
            running = false;
        }

        SDL_Delay (delayTime);
    }

    if (currentStep >= cutOff && cutOff != -1) {
        std::ofstream log ("run.txt", std::iostream::app);

        if (log.is_open () == true) {
            log << score << std::endl;
        }
        
        log.close ();
    }
}

/**
 * @fn  int main ( int argc, char* args[] )
 *
 * @brief   Main entry-point for this application.
 *
 * @param   argc    Number of command-line arguments.
 * @param   args    Array of command-line argument strings.
 *
 * @return  Exit-code for the process - 0 for success, else an error code.
 */

int main ( int argc, char* args[] ) {

	// initiate the random number generator with a seed based on time
	srand (time (0));

	display = new Display ("AI", SCREEN_WIDTH, SCREEN_HEIGHT);
	
    int choice = -1;

    // agent type via command line parameters 
    if (argc >= 2) {
        try {
            choice = std::stoi (args[1]);
        } catch (std::invalid_argument e) {
            choice = -1;
        }

    }

    // cutoff value via command line parameters 
    if (argc >= 3) {
        try {
            cutOff = std::stoi (args[2]);
        } catch (std::invalid_argument e) {
            cutOff = 1000;
        }
    }

    // tick delay value via command line parameters 
    if (argc >= 4) {
        try {
            delayTime = std::stoi (args[3]);
        } catch (std::invalid_argument e) {
            delayTime = 30;
        }
    }


    choice = (choice == -1) ? utility::getNumber ("AI to use", 1, 3) : choice;

    switch (choice) {
        case 1:
            agent = new SimpleReflexAgent ();
        break;

        case 2:
            agent = new RationalAgent ();
            randomDirt = true;
        break;

        case 3:
            agent = new ThinkAgent ();
            randomDirt = true;
        break;

        default:
            return EXIT_FAILURE;
        break;
    }

    map = new SquareMap (WORLD_WIDTH, WORLD_HEIGHT);
    derpMap = new SquareMap (WORLD_WIDTH, WORLD_HEIGHT);

	// controller creation, handles the map <-> agent interaction and the agents position in the world
	controller = new Controller (
		agent,
		map,
		std::pair<int, int> (WORLD_WIDTH / 2, WORLD_HEIGHT / 2)
	);

    derpAgent = new SimpleReflexAgent ();
    

    derpController = new Controller (
        derpAgent,
        derpMap,
        std::pair<int, int> (WORLD_WIDTH / 2, WORLD_HEIGHT / 2)
    );


    controller->setOffset (std::pair<int, int>(0, 0));
    derpController->setOffset (std::pair<int, int> (0, WORLD_HEIGHT + 1));

    loop();

	//cleanup
	delete display;
	delete controller;

	return EXIT_SUCCESS;
}
