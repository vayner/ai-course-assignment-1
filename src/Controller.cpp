/**
 * @file    src\Controller.cpp
 *
 * @brief   Implements the controller class.
 */

#include "Controller.h"
#include "Utility.h"

/**
 * @fn  Controller::Controller (Agent * agent, Environment * map, const std::pair<int, int> & pos)
 *
 * @brief   Constructor. Also sets Agent's worldAccess to itself.
 *
 * @param [in,out]  agent   If non-null, the agent.
 * @param [in,out]  map     If non-null, the map.
 * @param   pos             The agent's start position.
 */

Controller::Controller (Agent * agent, Environment * map, const std::pair<int, int> & pos) : WorldAccess (agent, map, pos) {
	agentTexture = display->loadTexture ("textures/robot.png");
    map->setFlat (agentPos, true);
    for (auto &dir:DIRECTIONS) {            //Give the robot at least some room to clean
        map->setFlat (agentPos+dir, true);
    }

    offset = {0, 0};
    displacement = {WORLD_WIDTH, 0};
}

/**
 * @fn  Controller::~Controller ()
 *
 * @brief   Destructor.
 */

Controller::~Controller () {
	delete map;
	delete agent;
}

/**
 * @fn  int Controller::nextStep ()
 *
 * @brief   Executes next logic step for the AI.
 *
 * @return  An int.
 */

int Controller::nextStep () {
	agent->nextStep ();
	return ++currentStep;
};

/**
 * @fn  bool Controller::generateRandomDirt ()
 *
 * @brief   Generates 1 random dirt. Will succeed
 *
 * @return  true if it succeeds, false if it fails.
 */

bool Controller::generateRandomDirt () {
    bool result;

    do {
        result = this->generateDirt (std::pair<int, int> (rand () % WORLD_WIDTH, rand () % WORLD_HEIGHT));
    } while (result == false);
    
    return result;
}

/**
 * @fn  bool Controller::generateDirt (const std::pair<int, int> & pos)
 *
 * @brief   Generates dirt at specified position.
 *
 * @param   pos The position.
 *
 * @return  true if it succeeds, false if it fails.
 */

bool Controller::generateDirt (const std::pair<int, int> & pos) {
    return map->setClean (pos, false);
}

/**
 * @fn  void Controller::draw (bool drawAgentSide)
 *
 * @brief   Calls agent and maps draw function and draws the agent.
 *
 * @param   drawAgentSide   true to draw agent side.
 */

void Controller::draw (bool drawAgentSide) {

	map->draw (this->offset);
	display->drawTileTexture (this->agentPos + this->offset, agentTexture);
    
    if (drawAgentSide == true) {
        agent->draw (this->offset + displacement);
        display->drawTileTexture (this->agentPos + this->offset + displacement, agentTexture);
    }
}

/**
 * @fn  void Controller::getOffset ()
 *
 * @brief   Gets the offset.
 */

const std::pair<int, int> & Controller::getOffset () const {
    return this->offset;
}

/**
 * @fn  void Controller::setOffset (const std::pair<int, int> & offset)
 *
 * @brief   Sets an offset.
 *
 * @param   offset  The offset.
 */

void Controller::setOffset (const std::pair<int, int> & offset) {
    this->offset = offset;
}