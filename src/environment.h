/**
 * @file    src\environment.h
 *
 * @brief   Declares the environment class.
 */

#pragma once

#include "NodeGrid.h"
#include "SDL.h"

/**
 * @struct  EnvironmentNode
 *
 * @brief   An environment node. represent one tile on the map
 */

struct EnvironmentNode {
    bool clean = true;
    bool flat = true;
};

/**
 * @class   Environment
 *
 * @brief   Represents the map and useful functions related to manipualting it. Initialy empty.
 */

class Environment {
public:

    /**
     * @fn  Environment::Environment();
     *
     * @brief   Default constructor.
     */

    Environment();

    /**
     * @fn  virtual Environment::~Environment ();
     *
     * @brief   Destructor.
     */

    virtual ~Environment ();

    /**
     * @fn  bool Environment::exists (const std::pair<int, int> & pos);
     *
     * @brief   Check if specified node exists at the given position.
     *
     * @param   pos The position.
     *
     * @return  true if it exists, false otherwise.
     */

	bool exists (const std::pair<int, int> & pos);

    /**
     * @fn  bool Environment::getClean (const std::pair<int, int> & pos);
     *
     * @brief   Gets the clean value for the specified position.
     *
     * @param   pos The position.
     *
     * @return  true if it is clean, false otherwise.
     */

	bool getClean (const std::pair<int, int> & pos);

    /**
     * @fn  bool Environment::setClean (const std::pair<int, int> & pos, bool clean = true);
     *
     * @brief   Sets the clean value for specified node. Will return false if none exists at that
     *          position.
     *
     * @param   pos     The position to set value for.
     * @param   clean   (Optional) Value to set clean to. Default true.
     *
     * @return  true if it succeeds, false if it fails.
     */

	bool setClean (const std::pair<int, int> & pos, bool clean = true);

    /**
     * @fn  bool Environment::getFlat (const std::pair<int, int> & pos);
     *
     * @brief   Gets the flat value for the specified position.
     *
     * @param   pos The position.
     *
     * @return  true if it is flat, false otherwise.
     */

	bool getFlat (const std::pair<int, int> & pos);

    /**
     * @fn  bool Environment::setFlat (const std::pair<int, int> & pos, bool flat);
     *
     * @brief   Sets the flat value for specified node. Will return false if none exists at that
     *          position.
     *
     * @param   pos     The position to set value for.
     * @param   flat    Value to set flat to.
     *
     * @return  true if it succeeds, false if it fails.
     */

	bool setFlat (const std::pair<int, int> & pos, bool flat);

    /**
     * @fn  virtual void Environment::draw (const std::pair<int, int> &offset);
     *
     * @brief   Draws the area.
     *
     * @param   offset  The offset.
     */

    virtual void draw (const std::pair<int, int> &offset);

    /**
     * @fn  void Environment::count ();
     *
     * @brief   Counts the number of dirt, clean and blocked tiles.
     */

    void count ();

    /**
     * @fn  int Environment::getDirtyNumber ();
     *
     * @brief   Gets dirty number.
     *
     * @return  The dirty number.
     */

    int getDirtyNumber ();

    /**
     * @fn  int Environment::getCleanNumber ();
     *
     * @brief   Gets clean number.
     *
     * @return  The clean number.
     */

    int getCleanNumber ();

    /**
     * @fn  int Environment::getBlocksNumber ();
     *
     * @brief   Gets blocks number.
     *
     * @return  The blocks number.
     */

    int getBlocksNumber ();

protected:

	NodeGrid<EnvironmentNode> * area;

	int tileTexture;
	int dirtTexture;
	int wallTexture;

private:

    int dirty = 0;
    int clean = 0;
    int blocks = 0;
};
