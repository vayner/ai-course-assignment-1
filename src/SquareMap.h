/**
 * @file    src\SquareMap.h
 *
 * @brief   Declares the square map class.
 */

#pragma once

#include "environment.h"

/**
 * @class   SquareMap
 *
 * @brief   Extends Environment to generate the map at construction
 *
 * @sa  Environment
 */

class SquareMap : public Environment {
public:

    /**
     * @fn  SquareMap::SquareMap (int width, int height);
     *
     * @brief   Constructor. Generates the map
     *
     * @param   width   The width.
     * @param   height  The height.
     */

	SquareMap (int width, int height);

private:

	int width;
	int height;
};