/**
 * @file    src\display.h
 *
 * @brief   Declares the display class.
 */

#pragma once

#include "SDL.h"
#include "SDL_image.h"
#include <string>
#include <vector>
#include <map>

/**
 * @class   Display
 *
 * @brief   A display class representing a window with its own renderer
 */

class Display {
    public:

        /**
         * @fn  Display::Display(const std::string & title, int width, int height);
         *
         * @brief   Constructor.
         *
         * @param   title   Window title
         * @param   width   Window with
         * @param   height  Window height
         */

        Display(const std::string & title, int width, int height);

        /**
         * @fn  virtual Display::~Display();
         *
         * @brief   Destructor.
         */

        virtual ~Display();

        /**
         * @fn  void Display::drawTexture (int x, int y, int textureID);
         *
         * @brief   Draw texture.
         *
         * @param   x           The x coordinate.
         * @param   y           The y coordinate.
         * @param   textureID   Identifier for the texture.
         */

		void drawTexture (int x, int y, int textureID);

        /**
         * @fn  void Display::drawTileTexture (int x, int y, int textureID, SDL_Color tint)
         *
         * @brief   Draw texture with grid coordinates. Grid size is the texture's size.
         *
         * @param   x           The x coordinate.
         * @param   y           The y coordinate.
         * @param   textureID   Identifier for the texture.
         * @param   tint        The tint to draw the texture with.
         */

		void drawTileTexture (int x, int y, int textureID, SDL_Color tint = {255,255,255,0});

        /**
         * @fn  void Display::drawTileTexture (const std::pair<int, int> &pos, int textureID, SDL_Color tint =
         *
         * @brief   Draw texture with grid coordinates. Grid size is the texture's size.
         *
         * @param   pos         The position.
         * @param   textureID   Identifier for the texture.
         * @param   tint        The tint to draw the texture with.
         */

        void drawTileTexture (const std::pair<int, int> &pos, int textureID, SDL_Color tint = {255, 255, 255, 0});

        /**
         * @fn  void Display::update ();
         *
         * @brief   Updates the windows visual content.
         */

		void update ();

        /**
         * @fn  int Display::loadTexture (const std::string & path);
         *
         * @brief   Loads a texture.
         *
         * @param   path    Full pathname of the texture file.
         *
         * @return  The texture Identifier.
         */

		int loadTexture (const std::string & path);

    protected:

        SDL_Window *window;
        SDL_Renderer *renderer;

    private:

		std::vector<SDL_Texture*> textures;
		std::map<std::string, int> texturePaths;
};