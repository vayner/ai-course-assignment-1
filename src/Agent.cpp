/**
 * @file    src\Agent.cpp
 *
 * @brief   Implements the agent class.
 */

#include "Agent.h"

/**
 * @fn  Agent::Agent ()
 *
 * @brief   Default constructor.
 */

Agent::Agent () {
}

/**
 * @fn  Agent::~Agent ()
 *
 * @brief   Destructor.
 */

Agent::~Agent () {
}

/**
 * @fn  void Agent::setAccess (WorldAccess * worldAccess)
 *
 * @brief   Sets the worldAccess reference.
 *
 * @param [in,out]  worldAccess If non-null, the world access.
 */

void Agent::setAccess (WorldAccess * worldAccess) {
	this->worldAccess = worldAccess;
}

/**
 * @fn  void Agent::draw (const std::pair<int, int> &offset)
 *
 * @brief   Draws this object. Currently empty, meant to be overridden.
 *
 * @param   offset  The offset.
 */

void Agent::draw (const std::pair<int, int> &offset) {
};