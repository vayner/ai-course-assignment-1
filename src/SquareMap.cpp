/**
 * @file    src\SquareMap.cpp
 *
 * @brief   Implements the square map class.
 */

#include "SquareMap.h"

#include "Global.h"

#include <cstdlib>

const int BLOCKS = 50;
const int DIRTY = 50;

/**
 * @fn  SquareMap::SquareMap (int width, int height)
 *
 * @brief   Constructor. Generates the map.
 *
 * @param   width   The width.
 * @param   height  The height.
 */

SquareMap::SquareMap (int width, int height) : width(width), height(height), Environment() {
    EnvironmentNode * data;
    
    for (int i = 0; i < width; i++) {
		for (int j = 0; j < height; j++) {
			data = new EnvironmentNode();

            data->clean = true;
            data->flat = true;

			this->area->addNode (std::pair<int, int> (i, j), data);
		}
	}

    for (size_t i = 0; i < BLOCKS; i++) {
        while (true) {
            data = this->area->getData (std::pair<int, int> (rand () % WORLD_WIDTH, rand () % WORLD_HEIGHT));

            if (data->flat == true) {
                data->flat = false;
                break;
            }
        }
    }


    for (size_t i = 0; i < DIRTY; i++) {
        while (true) {
            data = this->area->getData (std::pair<int, int> (rand () % WORLD_WIDTH, rand () % WORLD_HEIGHT));

            if (data->flat == true) {
                data->clean = false;
                break;
            }
        }
    }
}
